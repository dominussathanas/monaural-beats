# This is the Monaural Beats Shell Script for GNU / Linux ... ;
# This runs in Born Again Shell as ./ and bash ..... ;
# This is "as-is" with absolutely "no-warranty" and "n/a" ... ;
# The Shell Script Generates a Monaural Beats MP3 File ..... ;
# The File is at the Schuman Resonance at 0.5 with 126.22 ... ;
# 126.22 is used in the Band Sun-O a common Natural Freq ... ;
# This Project Requires GNU / Linux / ffmpeg / base32 / mp3 ... ;
https://sourceforge.net/projects/monaural-beats/files/
https://gitlab.com/mrfaildeveloper/monaural-beats/-/tree/master/
https://bitbucket.org/dominussathanas/monaural-beats/src/master/
https://archive.org/details/monaural-beats-sh/